import './App.css';
import { Unity, useUnityContext } from "react-unity-webgl";
import React from "react";

function App() {

  const { unityProvider } = useUnityContext({
    loaderUrl: "build/StellarGateWeb.loader.js",
    dataUrl: "build/StellarGateWeb.data",
    frameworkUrl: "build/StellarGateWeb.framework.js",
    codeUrl: "build/StellarGateWeb.wasm",
  });
  
  return (
    <Unity unityProvider={unityProvider} />
  );
}

export default App;
